import random

from db.postgres import get_connection
from db.data_collector import DataCollector

prepared_data = {
    'num': random.randint(0, 9),
    'data': 'string'
}

data_collector = DataCollector(get_connection())
data_collector.migrate()
data_collector.insert(prepared_data)

rows = data_collector.get_all()

for row in rows:
    print(row)

data_collector.close()

print('All done')
