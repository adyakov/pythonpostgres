
class DataCollector:
    def __init__(self, connection):
        self.connection = connection
        self.cursor = self.connection.cursor()

    def migrate(self):
        try:
            self.cursor.execute("CREATE TABLE IF NOT EXISTS test (id serial PRIMARY KEY, num integer, data varchar);")
            self.connection.commit()
        except:
            print("I can't create TABLE!")

    def insert(self, data):
        try:
            self.cursor.execute("INSERT INTO test (num, data) VALUES(%s, %s)", (data['num'], data['data']))
            self.connection.commit()
        except:
            print("I do not have an ability to insert that shit")

    def get_all(self):
        try:
            self.cursor.execute("SELECT num, data FROM test")
            data = self.cursor.fetchall()
            return data
        except:
            print("I do not have an ability to get that shit")

    def close(self):
        self.cursor.close()
        self.connection.close()

