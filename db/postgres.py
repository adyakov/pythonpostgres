import psycopg2

def get_connection():
    conn = psycopg2.connect(database="postgres",
                            host="localhost",
                            user="postgres",
                            password="postgrespass",
                            port="5432")
    return conn
